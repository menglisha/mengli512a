%% Econ 512 Homework 3 
% Mengli Sha
clear all
%% Question 1
% load file from hw3. mat
load('hw3.mat');
% set data as input for functions
global X  y
% I defined the loglikelihood function in loglike.m with a minus sign in
% front of it
[nsample,ndep]=size(X); %nsample is the number of samples(individuals) and ndep is the number of dependent variables
% initial guess 
beta0=zeros(ndep,1);
% now, use four algorithms
% first algorithm, Fminfunc without a derivative
options=optimoptions('fminunc','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs');
[beta_Alg1,f1val,exitflag1,output_Alg1]=fminunc(@loglike,beta0,options);
% second algorithm, Fminfunc with a derivative provided, this requires me
% to put the second argument in the loglike file as the derivative (by setting 'GradObj' to be 'on', so I
% define another function for this: loglikeder;
options = optimoptions('fminunc','GradObj','on','Algorithm','quasi-newton','HessUpdate','bfgs');
[beta_Alg2,f2val,exitflag2,output_Alg2,grad2,hessian2] = fminunc(@loglikeder,beta0,options);
% by the way I also try to use trust-region algorithm with a gradient
% supplied, but it seems that it is not working well
options=optimoptions('fminunc','Algorithm','trust-region');
[beta_trust,fvaltrust,exitflagtrust,output_trust]=fminunc(@loglikeder,beta0,options);
% third, Nelder Mead, which is the fminsearch
[beta_Alg3,f3val,exitflag3,output_Alg3]=fminsearch(@loglike,beta0);
% the result of fminsearch is so different, so I want to check whether it
% is because it stuck at a local maximum, by setting the inital value
% close to beta_Alg1;
% check: % it turns out that then this gives same result as other
% algorithms

% You just did not alter default value for number of Dunction evaluations.
% See answer key
beta_Alg3check=fminsearch(@loglike,beta_Alg1-0.1); 
% fourth, BHHH method which the updating rule involves score and gradient
% gradient of loglikelihood function, when grad=0, we say that we found a
% maximum 
% I found that if I set the tol tighter, instead of 0.001, it will be stuck
% into d getting very small: not really updating much. However, even i set
% 0.001 it seems that the result is same as algorithm 1 and 2.
% grad=@(b) -((exp(X*b))')*X+y'*X;  this is essentially score*1(601), in
% order to minimize the number of function evaluations, i express grad as
% score in the following estimation
for ite=1:2000
    beta_4(:,1)=beta0;
    for i=1:nsample
            score(i,:,ite)=-exp(X(i,:)*beta_4(:,ite))*X(i,:)+y(i)*X(i,:);
    end
    % one could simply write -X.*exp(X*b)+y.*X, some matlab version accepts
    % this element wise product, but my matlab reports error and say matrix
    % dimensions must agree. I can of course, redefine y to be a 601*6
    % matrix with every column the original y and then do the pointwise
    % product. I realized this after I have written the loops already, so
    % for this time I am not revising.
    grads(:,ite)=score(:,:,ite)'*ones(nsample,1);
    if max(abs(grads(:,ite)))<0.001
    %if max(abs(grad(beta_4(:,ite))))<0.001;
        disp('root found')
        beta_Alg4=beta_4(:,ite);
        break;
    else
        
        d=((score(:,:,ite)'*score(:,:,ite))^(-1))*score(:,:,ite)'*ones(nsample,1);
        beta_4(:,ite+1)=beta_4(:,ite)+d;
        if norm(d)<sqrt(eps)
            disp('iteration stuck without progress')
            break
        end
    end
end
% now report;
disp('estimated parameters')
t1=table(beta_Alg1,beta_Alg2,beta_Alg3,beta_Alg4)
Rowname={'Alg1';'Alg2';'Alg3';'Alg4'};
n_iteration=[output_Alg1.iterations;output_Alg2.iterations;output_Alg3.iterations;ite-1];
n_func_eval=[output_Alg1.funcCount;output_Alg2.funcCount;output_Alg3.funcCount;ite-1];
t2=table(n_iteration,n_func_eval,'RowNames',Rowname)
% for alg1-3, just use the output; 
% for algorithm 4,
% number of function evaluations: each loop evaluate once calculating the
% score, everything else (gradient) is also based on score, so each
% iteration only invloves one evaluation. 

%% Question 2
% by information equality, E(score(i)score(i)')=-E(hessian), so 
% I use the score'score as the approximation for hessian here
% initial hessian
Hes_initial=-score(:,:,1)'*score(:,:,1);
e_ini=eig(Hes_initial)
Hes_est=-score(:,:,end)'*score(:,:,end);
e_est=eig(Hes_est)

%% Question 3
% now, NLLS,so different objective function
% resdiual sum of squares: I added a 0.5 in front of the original obj
% function, which does not matter but the updating rule etc will not have 2
% in it.Line 98 is even not used in the code, just to show what the
% objective function is.(can comment this out)
rs= @(b) 0.5*(y-exp(X*b))'*(y-exp(X*b));
% let f(b)=y-exp(X*b), we have the gradient of the objective function to be
% Trans(f'(b))*f(b) and the updating rule to be
% d=-(f'(b)Tf'(b))^(-1)f'(b)Tf(b) where AT means the transpose of A, so we
% need to calculate f'(b) and f(b), where f'(b) is the jacobian we defined in class, I put it
% in every loop
fb=@(b) y-exp(X*b);
%gradRS =@(b) (exp(X*b).*exp(X*b))'*X-(y.*exp(X*b))'*X;% this is essencially Jacobian(b)Tf(b)
for ite=1:2000
    beta_RS(:,1)=beta0;
    % calculate Jacobian [df1/dbeta;df2/dbeta;....;dfn/dbeta], where dfi/dbeta
     % is a row vector (just for notation ease)
        b=beta_RS(:,ite);
        for i=1:nsample
            J(i,:,ite)=-exp(X(i,:)*b)*X(i,:);
        end
   gradrs(ite,:)=J(:,:,ite)'*fb(b);
    if max(abs(gradrs(ite,:)))<0.0001
        disp('root found')
        beta_NLLS=beta_RS(:,ite);
        break;
    else
       %d=-(J(:,:,ite)'*J(:,:,ite))^(-1)*(gradRS(beta_RS(:,ite)))';
        d=-(J(:,:,ite)'*J(:,:,ite))^(-1)*(gradrs(ite,:)');
        beta_RS(:,ite+1)=beta_RS(:,ite)+d;
        if norm(d)<sqrt(eps)
            disp('iteration stuck without progress')
            break
        end
    end
end
% to check the accuracy of this estimate, 
options = optimoptions('fminunc','GradObj','on','Algorithm','quasi-newton','HessUpdate','bfgs');
[beta_NLLScheck] = fminunc(@residual,beta0,options);
% so for my NLLS method here, if I set tol tighter than 0.0001, d converges
% to zero before the gradient converge to zero. so I set my tol to be
% 0.0001 but check that if i use fminunc, I get the same result. One can
% always set another loop under d saying that if d converges to zero (not
% updating much), but if gradient is close to zero, we also regard this as
% a solution. However, this is equivalent to setting tol to be looser at an
% earlier stage.
% Report
disp('estimated parameter  number of iterations  function evaluations')
beta_NLLS
NLLSiter=ite-1
% number of function evaluations is just for each iteration once for fb and
% once for jacobian, twice, so (ite-1)*2
NLLSfeval=2*(ite-1)
