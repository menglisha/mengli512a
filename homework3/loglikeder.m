% the (negative) loglikelihood function WITH derivative supplied as the
% second component of the function,
%X from data (exogenous variables); arrange XX such that each row is a
%    sample (an individual profile)
%y from data:  the dependent variable
%input: b, the parameters, input as a column vector
%NOTE that the # col of XX should meet the dimension of beta
%output: loglikehood value (a scalar)
function [Logl,g]=loglikeder(b)
global X y nsample ndep
 Logl=-(-sum(exp(X*b))+sum(y.*(X*b))-sum(log(factorial(y))));
    if nargout>1 % the gradient
        g=-(-((exp(X*b))')*X+y'*X);
    end
end