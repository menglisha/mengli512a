% the (negative) loglikelihood function without derivative supplied
%X from data (exogenous variables); arrange XX such that each row is a
%    sample (an individual profile)
%y from data:  the dependent variable
%input: b, the parameters, input as a column vector
%NOTE that the # col of XX should meet the dimension of beta
%output: loglikehood value (a scalar)
function L=loglike(b)
global X y nsample
   L=-(-sum(exp(X*b))+sum(y.*(X*b))-sum(log(factorial(y))));
end