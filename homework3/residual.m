% the function has two output: the residual sum of squares RS, and the
% gradient gRS
function [RS, gRS]=residual(b)
global X y
RS=(y-exp(X*b))'*(y-exp(X*b));
    if nargout>1
        gRS =2*(exp(X*b).*exp(X*b))'*X-2*(y.*exp(X*b))'*X; %gradient
    end
end

