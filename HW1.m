%% HW1 512a Mengli Sha
clear all
%%
%%Q1
%defining vector X
X=[1,1.5,3.5,4.78,5,7.2,9,10,10.1,10.56];
%defining y1 and y2
Y1=-2+0.5.*X;
Y2=-2+0.5.*(X.^2);
plot(X,Y1,X,Y2)

%%
%%Q2
%defining vector X, I name it X2 here to distinguish with X in Q1
X2=linspace(-20,40,120)
%now calculate the sum of the elements of the vector
s=sum(X2)

%%
%%Q3
%first define everything that is directely defined
A=[3,4,5;2,12,6;1,7,4];
b=[3;-2;10];
%Now the required calculation
C=A'*b
D=(inv(A'*A))*b
%E is actually A'*b, after expanding everything out it is clear to see. but
%I guess here you want us to do loop and see whether E=sum of elements in C, so I wrote a loop
E=0;
for i=1:3
    for j=1:3
        E=E+A(i,j)*b(i);
    end
end
E
F=[A(1,1:2);A(3,1:2)]
%since A is not singular, we have x=A^(-1)*b, this is how I solve the
%linear system Ax=b
x=inv(A)*b

%%
%%Q4 
%B is just the kronecker product of an identity matrix and A.
B=kron(eye(5,5),A)

%%
%%Q5
%I create matrix of random draws; to distinguishi with A defined above, I
%call this matrix A5
A5=normrnd(6.5,3,5,3);
%Now convert A to a matrix ConvA with only elements 0,1 under specific
%conditions.
for i=1:5
    for j=1:3
        if A5(i,j)<4
            ConvA(i,j)=0;
        else 
            ConvA(i,j)=1;
        end
    end
end
disp(A5)
disp(ConvA)

%%
%%Q6
%I do OLS with pooled data
%import data from csv file
Data=readtable('datahw1.csv');
% When I try on my computer, it doesnt work. You should have specified 'ReadVariableNames',false
D=[Data.Var1,Data.Var2,Data.Var3,Data.Var4,Data.Var5,Data.Var6];
%check for missing data, if data is missing, drop the observation
delete=0
for j=1:6
    for i=1:length(D)
        if isnan(D(i,j))==1
           delete=[delete,i];
        end
    end
end
m=0
for k=2:length(delete)
    D(delete(k)+m,:)=[];
    m=m-1;
    %%since each time we delete one row, the number of row afterwards 
    %%decreases by 1,meaning, we need to adjust the number of row to be
    %%deleted accordingly.
end
%Now display how many missing data are dropped
numdeleted=length(delete)-1;
disp(['number of data deleted:',num2str(numdeleted)])
%imput each variable
Export=D(:,3);
prod=D(:,5);
RD=D(:,4);
cap=D(:,6);
one=ones(length(Export),1);
%define the independent variables 
X=[one,Export,RD,cap];
%the following command reports point estimates in as a vector.
beta=regress(prod,X)
%check the estimates with the usually OLS form
betaols=(X'*X)^(-1)*X'*prod
