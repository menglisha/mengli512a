function result=MC(bsg,node)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% this function calculates the -logL using monte carlo depending on the number of nodes
% INPUT: bs=[bb;ss;gamma] a column vector
%        (1)bb: b0 in the model
%        (2)ss: sigmab in the model
%        (3)gamma: gamma in the model
%        node: number of node of monte carlo
% OUTPUT: logL 
global T N X Y Z
gamma=bsg(3);
rng(2)
%nod is defined in the main script, =20
bm=normrnd(bsg(1),bsg(2),[node,1]);
for j=1:length(bm)
   % betai=kron(ones(T,1),ones(1,N)*bm(j));
    bxz=bm(j)*X+gamma*Z;
    lt=((1+exp(-bxz)).^(-1)).^(Y).*((1-((1+exp(-bxz)).^(-1))).^(1.-Y));
    Likebm(j,:)=prod(lt);%returens the product of elements in every column, so we are producting over T
end
integralLiMC=sum(Likebm)/node; %we had 20 draws for each person and we take average over 20 for each person
logLMC=sum(log(integralLiMC),2);
result=-logLMC;