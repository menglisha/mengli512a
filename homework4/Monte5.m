function result=Monte5(input,node)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% note that in the question, u0=sigmau=sigmabu, which reduces significantly
% the dimension of the problem. so i just use u0 as the input.
% this function calculates the -logLikelihood function use monte carlo,
% allowing u0=sigmau=sigmabu<>0.
% INPUT: input is a vector =[b0;sigmab;u0;gamma]
% OUTPUT: - logLikelihood
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global X Y N T Z
b0=input(1);
sigmab=input(2);
u0=input(3);
gamma=input(4);
sigmabu=u0;
sigmau=u0;
Bigsigma=[sigmab,sigmabu;sigmabu,sigmau];
% generate correlated multivariate normal
rng(3)
% I set the node number to be as in question 3 here
% generate independent ones first
buraw= normrnd(0,1,[node,2]);
% use chol to make them correlated
busimu=kron(ones(node,1),[b0,u0])+buraw*chol(Bigsigma);
bdraw=busimu(:,1);
udraw=busimu(:,2);
for j=1:node
    ui=kron(ones(T,1),ones(1,N)*udraw(j));
    bxzu=bdraw(j)*X+gamma*Z+ui;
    lt=((1+exp(-bxzu)).^(-1)).^(Y).*((1-((1+exp(-bxzu)).^(-1))).^(1.-Y));
    Like(j,:)=prod(lt);%returns the product of elements in every column, so we are producting over T
end
integralMC=sum(Like)/node; %we had 20 draws for each person and we take average over 20 for each person
logLikeliMC=sum(log(integralMC),2);
result=-logLikeliMC;
end