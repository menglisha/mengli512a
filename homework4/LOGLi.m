function l=LOGLi(b) 
%%%%%%%%%%%%%%%%%%%%%%%%%
% USAGE: to calculate Li
% INPUT: 
    % b (a column vector (#node x 1) column vector)
% OUTPUT:
    % Li (a (#nodexN) vector, row vector, each element is Li)
%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate matrix of bi to do elementwise product with Xit
global T X Y N
% each columne of b
for j=1:length(b)
    betai=kron(ones(T,1),ones(1,N)*b(j));
% betai*Xit:name it as bx
    bx=betai.*X;
% notice that it is easier to calculate logLi first than do Li directly
% so logLi=sum[Yit*logF+(1-Yit)*log(1-F)]
% define function F
    F= @(a) (1+exp(-a)).^(-1);
    lt=Y.*log(F(bx))+(1.-Y).*log(1-F(bx))
    LogLi(j,:)=sum(lt);
end
l=LogLi;
end