%% Empirical Methods, Homework4  
%  Mengli Sha Dec 2016
clear all
%% Question 1
% First of all, load the data file 
data=load('hw4data.mat');
% take the segments from data 
global X Y Z N T nod
X=data.data.X;
Y=data.data.Y;
Z=data.data.Z;
N=data.data.N;
T=data.data.T;
% parameter values for question 1
% for gamma and u, I just simply don'e include them in question 1 since
% they are zero
b0=0.1;
sigmab=1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2: Second, calculate L
% Gaussian Quadrature
% need to draw 5 bi's for N people, but since bi are drawn from the same
% distribution, everyone in the node will have same bi's
[b1,w1]=qnwnorm(5,b0,sigmab); % this gives all i's the same b's
integralLi_5=w1'*Li(b1);
% the line above returns us a row vector, with each elemen to be the
% integral of i
% it turns out that calculating L itself it always give you zero (cuz we
% mutiply something smaller than 1 over 100 times. SO I use logL instead of
% L. so logL=sum(log(intrgralLi))
logL_5=sum(log(integralLi_5),2); %  L with 5 nodes
%% Question 2
% it is just to repeat with 10 nodes
[b2,w2]=qnwnorm(10,b0,sigmab);
integralLi_10=w2'*Li(b2);
logL_10=sum(log(integralLi_10),2);
%% Question 3
% to do Monte Carlo, need to draw some random bi first
nod=20
rng(2)
% now I draw betas, for simplicity and also (since they are drawn from the
% same distribution, WLOG), I let everyone have the same beta.
b3=normrnd(b0,sigmab,[nod,1]);
for j=1:length(b3)
    bx=b3(j)*X;
    lt=((1+exp(-bx)).^(-1)).^(Y).*((1-((1+exp(-bx)).^(-1))).^(1.-Y));
    Likeb3(j,:)=prod(lt);%returns the product of elements in every column, so we are producting over T
end
integralLi_Q3=(1/nod)*sum(Likeb3); %we had 20 draws for each person and we take average over 20 for each person
logL_Q3=sum(log(integralLi_Q3),2);

%% Question 4
% in question 4 and 5, we allow gamma to be nonzero, and it is also to be
% estimated.
Question1=@(bsg) GQ(bsg,5);
Question2=@(bsg) GQ(bsg,10);
Monte=@(bsg) MC(bsg,nod);
% the constraint here is just a lower bond of sigma to be zero 
bsg0=[0.1;1;0.1];
A=[];
b=[];
Aeq=[];
beq=[];
ub=[100;100;100];
lb=[-100;0;-100];
[bsg_1,fval_1]=fmincon(Question1,bsg0,A,b,Aeq,beq,lb,ub);
[bsg_2,fval_2]=fmincon(Question2,bsg0,A,b,Aeq,beq,lb,ub);
% the command is modified because putting the constraint is literally a 
[bsg_m,fvalm]=fmincon(Monte,bsg0,A,b,Aeq,beq,lb,ub);
%[bsg_m,fvalm]=fmincon(Monte,bsg0,A,b);%this option
%setting gives exactly the same result as the one above

%% Question 5
Question5=@(input) Monte5(input,1000);
% about the 1000: I draw 1000 draws because I found that drawing only 100
% or 500 from rng(2) and rng(3) (for example) gives me different results of
% estimates which is simulation error. So I tried 1000 then the estimates
% are close to each other. I think the more draws the better, but we of
% course want to economize the computation time. Having 1000 draws adds to
% the computational time quite a bit: so the whole .m files runs a few
% minutes)
% now constraints are, (for BIGsigma to be positive definite) the principal
% minors need to be positive
% (1) u0 nonnegative 
% (2) sigmab nonnegative, 
% (3) sigmab*sigmau>sigmabu^2, and cancelout sigmabu,this is a constraint 
% that sigmab>u0 (note that u0=sigmabu);
% input is in the form of input=[b0,sigmab,u0,gamma]
A5=[0,0,-1,0;0,-1,0,0;0,-1,1,0];
b5=[0;0;0];
%input0=[0.1;1;0.5;0.1];
input0=[1;1;0.5;0];
[input,fval5]=fmincon(Question5,input0,A5,b5);

%% Report
disp('estimation results')
Lastname={'GQ_5','GQ_10','MC','MC with u'};
beta0_r=[bsg_1(1);bsg_2(1);bsg_m(1);input(1)];
sigmab_r=[[bsg_1(2);bsg_2(2);bsg_m(2);input(2)]];
u0_r=[0;0;0;input(3)];
gamma_r=[bsg_1(3);bsg_2(3);bsg_m(3);input(4)];
logfval_r=[-fval_1;-fval_2;-fvalm;-fval5];
table(beta0_r,sigmab_r,u0_r,gamma_r,logfval_r,'RowNames',Lastname)
disp('initial values: b0=0.1,sigma=1;u0=0.5;gamma=0.1 ')

 
