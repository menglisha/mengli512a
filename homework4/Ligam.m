function l=Ligam(bg) 
%%%%%%%%%%%%%%%%%%%%%%%%%
% USAGE: to calculate Li with non zero gamma
% INPUT: a column vector bg, which composts of [b;gamma]
    % b ( (#node x 1) column vector) generated by qnwnorm,
    % gamma, a number
% OUTPUT:
    % Li (a (#nodexN) matrix, each row vector corresponds to one experiment, each element of a row is Li)
%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate matrix of bi to do elementwise product with Xit
global T X Y N Z
lbg=length(bg);
b=bg(1:length(bg)-1);
gamma=bg(end);
for j=1:length(b)
    %betai=kron(ones(T,1),ones(1,N)*b(j));
    bxz=b(j)*X+gamma*Z;
    lt=((1+exp(-bxz)).^(-1)).^(Y).*((1-((1+exp(-bxz)).^(-1))).^(1.-Y));
    Likeb(j,:)=prod(lt);%returens the product of elements in every column, so we are producting over T
end
l=Likeb;