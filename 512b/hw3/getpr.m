%% Econ 512B Problem Set 3 Duopoly 
% Mengli Sha Feb 24 2017
% this script calculates (pr(w'|w,q)):
% =================================================
% Dimen    1 W tomorrow state;
%          2 w today state;
%          3 q either made a sale or not 0 or 1;
% pr(w'|w,q) LxLx2
% =================================================
global L rho kap l v del b C eta
getpr=zeros(L,L,2);
wgrid=1:1:L;
deltaw= 1-(1-del).^wgrid;
% now, for the first 3rd dimension (q=0)
for i=2:L-1
    for qplusone=1:2
        q=qplusone-1
    getpr(q+i,i,qplusone)=1-deltaw(i);
    getpr(q+i-1,i,qplusone)=deltaw(i);
    end
end
% fixing w=L and w=1
getpr(L,L,2)=1;            %pr(L,L,q=1)=1
getpr(L,L,1)=1-deltaw(L);  %pr(L,L,q=0)=1-delta(L)
getpr(L-1,L,1)=deltaw(L);  %pr(L-1,L,q=0)=delta(L);
getpr(1,1,1)=1;            %pr(1,1,q=0)=1
getpr(1,1,2)=deltaw(1);    %pr(1,1,q=1)=delta(1);
getpr(2,1,2)=1-deltaw(1);   %pr(2,1,q=1)=1-delta(1);
% until now, getpr calculation is finished.
save getpr.mat getpr
% so getpr(:,:,1) corresponds to q=0; getpr(:,:,1) corresponds to q=1


