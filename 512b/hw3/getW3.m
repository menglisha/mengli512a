%% Econ 512B Problem Set 3 Duopoly 
% Mengli Sha Feb 24 2017
% this script calculates W(w1,w2,sales)
% =================================================
% INPUT:    V: the V from the last iteration
% OUTPUT:   Wk(w1,w2,3d): first dimension corresponds to k;
%           so 3d=1 == k=0; 3d=2 == k=1; 3d=3 == k=2;
% =================================================
function W=getW3(V)
global L rho kap l v del b C eta getpr
longV=reshape(V,900,1);
WW=77*ones(L,L,3);
trans0=getpr(:,:,1)';
trans1=getpr(:,:,2)';
% k==0 q1=q2=0;
junk1=reshape(kron(trans0,trans0)*longV,L,L);
WW(:,:,1)=junk1;
% k==1 q1=1,q2=0;
junk2=reshape(kron(trans0,trans1)*longV,L,L);
WW(:,:,2)=junk2;
% k==2 q1=0, q2=1
junk3=reshape(kron(trans1,trans0)*longV,L,L);
WW(:,:,3)=junk3;
W=WW;
end