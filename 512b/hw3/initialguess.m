%% Econ 512B Problem Set 3 Duopoly 
% Mengli Sha Feb 24 2017
% this script just calculates a rough initial guess: I calculate pi as if
% it is just a one shot game and suppose p1==p2 always, so collaps to p
global L rho kap l v del b C eta
real_pi=zeros(L,1);
p0=zeros(L,1);
for i=1:L
profit=@(p) -((exp(v-p)/(1+2*exp(v-p)))*(p-C(i)));% this is negative profit!
p0(i)=fminsearch(profit,0.1);
real_pi(i)=-profit(p0(i));
end
V0=real_pi./(1-b);
Vini=repmat(V0,1,L);
pini=repmat(p0,1,L);