%% Econ 512B Problem Set 3 Duopoly 
% Mengli Sha Feb 24 2017
% this script returns f.o.c.
% =================================================
%           
% OUTPUT:   p1 matrix LxL
% =================================================
function F=foc(p)
global L rho kap l v del b C eta V1 p2 I J
V2=V1';
F=77*ones(L,L);
f=77*ones(L,L);
w1=I;
w2=J;
D1=exp(v-p)/(1+exp(v-p)+exp(v-p2(w1,w2)));
D2=exp(v-p2(w1,w2))/(1+exp(v-p)+exp(v-p2(w1,w2)));
D0=1-D1-D2;
f=1-(1-D1)*(p-C(w1))-b*getW(w1,w2,1,V1)+b*(getW(w1,w2,1,V1)*D1+getW(w1,w1,2,V1)*D2+getW(w1,w2,0,V1)*D0);
F=f;
end
       