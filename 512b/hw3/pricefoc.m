%% Econ 512B Problem Set 3 Duopoly 
% Mengli Sha Feb 24 2017
% this script returns f.o.c. in a matrix
% =================================================
% INPUT:    p1
% OUTPUT:   f.o.c matrix LxL
% =================================================
function z=pricefoc(p1)
global L rho kap l v del b C eta V1 I J p2 junk
% first calculate Dn(w1,w2): matrix
D1foc=exp(v-p1)./(ones(L,L)+exp(v-p1)+exp(v-p2));
D2foc=exp(v-p2)./(ones(L,L)+exp(v-p1)+exp(v-p2));
D0foc=ones(L,L)-D1foc-D2foc;
% calculate Wk's
        W1foc=junk(:,:,2);
        W0foc=junk(:,:,1);
        W2foc=junk(:,:,3);

z=ones(L,L)-(ones(L,L)-D1foc).*(p1-repmat(C,1,L))-b*W1foc+b*(D0foc.*W0foc+D1foc.*W1foc+D2foc.*W2foc);
end

