%% Econ 512B Problem Set 3 Duopoly 
% Mengli Sha Feb 24 2017
clear all
global L rho kap l v del b C eta getpr
%% Setup Parameters
L=30;               % know-how levels;
rho=0.85;           % slope of learning curve;
kap=10;             % MC with minimal know-how;
l=15;               % threshold of learnning curve flatterning out
v=10;               % quality of the good
del=0.03;           % depreciation rate
b=1/1.05;           % discount factor
eta=log(rho)/log(2);% learning curve param   
lamb=1;             % dampening smooth paramter;
% Cost function
C=zeros(L,1);
C(1:l-1)=kap*[1:(l-1)].^eta;
C(l:L)=kap*l^eta;
% end of cost function specification

%===============================================
alreadypr=0; % 1 already had getpr, 0 calculate pr again
if alreadypr==0
    %====calculation of pr==========================
        getpr=zeros(L,L,2); % w',w,q
    wgrid=1:1:L;
    deltaw= 1-(1-del).^wgrid;
    % now, for the first 3rd dimension (q=0)
    for i=2:L-1
        for qplusone=1:2
            q=qplusone-1;
    getpr(q+i,i,qplusone)=1-deltaw(i);
    getpr(q+i-1,i,qplusone)=deltaw(i);
        end
    end
% fixing w=L and w=1
    getpr(L,L,2)=1;            %pr(L,L,q=1)=1
    getpr(L,L,1)=1-deltaw(L);  %pr(L,L,q=0)=1-delta(L)
    getpr(L-1,L,1)=deltaw(L);  %pr(L-1,L,q=0)=delta(L);
    getpr(1,1,1)=1;            %pr(1,1,q=0)=1
    getpr(1,1,2)=deltaw(1);    %pr(1,1,q=1)=delta(1);
    getpr(2,1,2)=1-deltaw(1);   %pr(2,1,q=1)=1-delta(1);
%=======until now, getpr calculation is finished.===============
    save getpr.mat getpr
else
    load getpr.mat;
end


%% VFI==============================================================
vfi=0;
if vfi==1 %if vfi=1, do vfi; otherwise just load the already converged value function and policy function;
    
% value function is V(w1,w2) so I consider that I am solving a matrix
% each row corresponds to my w w1, each column corresponds to opponent
% w w2;
% v(w1_1:w1_L w2_1) v(w1_1:w1_L w2_2)...., v(w1_1:w1_L w2_L)
initialguess % initialguess returns Vini (LxL) and pini(LxL);

% then the opponent's V and p is just the transpose of mine;
% Vini=zeros(L,L);
% pini=zeros(L,L);
maxiter=1000;
% so I have V1 and p1 from initial guess
p1=pini;
for iter=1:maxiter;
    iter 
    global V1 p2 junk
    p2=p1';
    V1=Vini;
    junk=getW3(V1);

            p1_new=fsolve(@pricefoc,p1);

    % now calculate V new
    Vnew=77*ones(L,L); % initiallize, set 77 so can easily check whether there are mistakes
        D1new=exp(v-p1_new)./(ones(L,L)+exp(v-p1_new)+exp(v-p2));
        D2new=exp(v-p2)./(ones(L,L)+exp(v-p1_new)+exp(v-p2));
        D0new=ones(L,L)-D1new-D2new;
        % calculate Wk's
        W1new=junk(:,:,2);
        W0new=junk(:,:,1);
        W2new=junk(:,:,3);
    Vnew=D1new.*(p1_new-repmat(C,1,L))+b*(D0new.*W0new+D1new.*W1new+D2new.*W2new);
    Vnow=Vnew(10,10)
    % check for convergence now;
    if max( max(max(abs((Vnew-V1)./(1+V1)))),  max(max(abs((p1_new-p1)./(1+p1)))))<1e-5;

        disp('VFI converged')
        save pstar.mat p1_new;
        save vstar.mat Vnew;
        break % my code converged at iter 684
        
    else
        % Vini is the old guess, V_l;
        Vini=Vnew*lamb+(1-lamb)*Vini; 
        p1=p1_new*lamb+(1-lamb)*p1;
        clear V1 p2
    end
end
else
    load vstar 
    load pstar
end

% plot policy and value function
figure(1);
mesh(Vnew);
title('Value Function');

figure(2);
mesh(p1_new);
title('Policy Function');
keyboard

%% Simulation (Question 2, distribution of states from state (1,1)
N=10000;
T=30;
rng(5);
shock=rand(1,T);% sale shock;
idio=rand(2*N,T);% idiocyncratic w shock
%load pstar.mat vstar.mat
p2policy=p1_new';
w_sim=55*ones(2*N,T);% each column is the state at time t
w_sim(:,1)=1;
p1_sim=7*ones(N,T);
p2_sim=7*ones(N,T);
for i1=1:N
        i2=i1+N; %so I take (i,N+i) as a pair
        for t=1:T
            s1=w_sim(i1,t);
            s2=w_sim(i2,t);
        p1_sim(t)=p1_new(s1,s2);
        p2_sim(t)=p2policy(s1,s2);
        Demand1=exp(v-p1_sim(t))/(1+exp(v-p1_sim(t))+exp(v-p2_sim(t)));
        Demand2=exp(v-p2_sim(t))/(1+exp(v-p1_sim(t))+exp(v-p2_sim(t)));
        % I calculate q according to following:
        % if shock<=Demand1, then 1 made the sale; if
        % Demand1<shock<=Demand2, then 2 made the sale; otherwise the
        % outside option made the sale. So each has probability
        % corresponding to Demand (1,2,0).
        if shock(t)<=Demand1
            q1=1; 
            q2=0;
        elseif shock(t)>Demand1 & shock(t)<=(Demand2+Demand1)
            q1=0;
            q2=1;
        elseif shock(t)>(Demand2+Demand1)
            q1=0;
            q2=0;
        end
        % this fixes when s=L and q=1========
         if s1+q1==31 
            pr1=1;
            w_sim(i1,t+1)=L;
         elseif s1==1 & q1==0;
             w_sim(i1,t+1)=1;
        else pr1=getpr(s1+q1,s1,q1+1);
            if idio(i1,t)<=pr1% remember q0 corresponds to 3d index=1; q0=1 corresponds to 3d index=2
            w_sim(i1,t+1)=s1+q1;
            else 
            w_sim(i1,t+1)=s1+q1-1;
            end% remember q0 corresponds to 3d index=1; q0=1 corresponds to 3d index=2
         end
        
        if s2+q2==31
            pr2=1;
            w_sim(i2,t+1)=L;
        elseif s2==1 & q2==0
            w_sim(i2,t+1)=1;
        else pr2=getpr(s2+q2,s2,q2+1);% remember q0 corresponds to 3d index=1; q0=1 corresponds to 3d index=2
            if idio(i2,t)<=pr2
            w_sim(i2,t+1)=s2+q2;
            else 
            w_sim(i2,t+1)=s2+q2-1;
            end
        end
        % above fixes when s=L and q=1=========
        
        
        end
end
frac=zeros(L,L,T);
for ind1=1:L
    for ind2=1:L
        for t=1:T
            count=length(find(w_sim(1:N,t)==ind1 & w_sim(N+1:end,t)==ind2))/N;
            frac(ind1,ind2,t)=count;
        end
    end
end
% now plotting distributions at T=10, 20, 30
figure(3)
mesh(frac(:,:,10));
title('Dist at T=10');

figure(4)
mesh(frac(:,:,20));
title('Dist at T=20');

figure(5)
mesh(frac(:,:,30));
title('Dist at T=30');

%% Stationary Distribution
% so it is just to repeat the exercise above until it converged
maxi=20000;
N=10000;
rng(5);
dmdshck=rand(1,maxi);
idio=rand(2*N,maxi);
w_stat=ones(2*N,1);
check=1;
Frac=frac(:,:,T);

for ite=1:maxi
    ite
    for i1=1:N
        i2=i1+N; %so I take (i,N+i) as a pair
            s1=w_stat(i1);
            s2=w_stat(i2);
        p1_s=p1_new(s1,s2);
        p2_s=p2policy(s1,s2);
        Demand1=exp(v-p1_s)/(1+exp(v-p1_s)+exp(v-p2_s));
        Demand2=exp(v-p2_s)/(1+exp(v-p1_s)+exp(v-p2_s));
        % I calculate q according to following:
        % if shock<=Demand1, then 1 made the sale; if
        % Demand1<shock<=Demand2, then 2 made the sale; otherwise the
        % outside option made the sale. So each has probability
        % corresponding to Demand (1,2,0).
        shck=dmdshck(ite);
        if shck<=Demand1
            q1=1; 
            q2=0;
        elseif shck>Demand1 & shck<=(Demand2+Demand1)
            q1=0;
            q2=1;
        elseif shck>(Demand2+Demand1)
            q1=0;
            q2=0;
        end
        %==this fixes q=1 when s=L, and q=0 when s=1
        if s1+q1==31 
            pr1(i1,ite)=1;
            w_stat(i1)=30;
        elseif s1==1 & q1==0;
            w_stat(i1)=1;
        else pr1(i1,ite)=getpr(s1+q1,s1,q1+1);
                if idio(i1,ite)<=pr1(i1,ite)
                w_stat(i1)=s1+q1;
                else 
                w_stat(i1)=s1+q1-1;
                end
        end
            % remember q0 corresponds to 3d index=1; q0=1 corresponds to 3d index=2
        if s2+q2==31
            pr2(i1,ite)=1;
            w_stat(i2)=30;
        elseif s2==1 & q2==0
            w_stat(i2)=1;
        else pr2(i1,ite)=getpr(s2+q2,s2,q2+1);% remember q0 corresponds to 3d index=1; q0=1 corresponds to 3d index=2
            if idio(i2,ite)<=pr2(i1,ite)
                w_stat(i2)=s2+q2;
            else 
                w_stat(i2)=s2+q2-1;
            end
        end
    end
    
    frac_s=100*ones(L,L);
    for ind1=1:L
        for ind2=1:L
            count=length(find(w_stat(1:N)==ind1 & w_stat(N+1:end)==ind2))/N;
            frac_s(ind1,ind2)=count;
        end
    end
    
    
    check=max(abs(max(frac_s-Frac)))
    if check<0.002
        disp('stationary dis found');
        break
    else
    Frac=frac_s;
    end
end
save stationdis.mat Frac
figure(6)
mesh(Frac);
title('stationary distribution');


    

