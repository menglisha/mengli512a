%% Econ 512b Problem set 2
% Mengli Sha Feb 2017
clear all
% parameters
d=0.95; % the discount factor


% shock from p
% notice that tauchen.m in the repository does not have a constant term p0,
% so in order to accomodate p0, I can just assume u is mean p0 normal
% distribution with sigma_u
sigma_u=0.1;
p0=0.5; % this is also the mean of u for the tauchen code
N=21; 
%N=5;
rho=0.5;
[prP,gridP,invdistP]=tauchen(N,p0,rho,sigma_u);

% state variable: tree stock A and price p
gridA=0:0.5:100;
nA=length(gridA);
nP=length(gridP);
% choice A', which still lives on A
% Bellman equation
% V(A,P)=max P(A-A')-0.2*(A-A')^1.5+expV(A',P')

%%% Question 1,2,3 solving the firms' problem: VFI
I=repmat(gridA,nA,1); % this is today's state A, each column is a state
J=repmat(gridA',1,nA); % this is today's choice A, for each column, we choose among rows
% so to be clear, my state A lives on each column, and P lives on the third
% dimension

% Period return
periodr=ones(nA,nA,nP);
   for i=1:nA
        for j=1:nA
            if I(i,j)<J(i,j)
                cost(i,j)=1e10;
            else
                cost(i,j)=0.2*((I(i,j)-J(i,j))^1.5);
            end
        end
    end
for pi=1:nP
    periodr(:,:,pi)=gridP(pi)*(I-J)-cost;
end
% now calculate the discounted expected value function, to do that, I have
% an initial guess
Vini=ones(nA,nA,nP);
V=max(Vini);
junk=zeros(1,nA,nP);
maxiter=1000;
for ite=1:maxiter
    for i=1:nP
        for j=1:nP
            junk(1,:,j)=prP(i,j)*V(1,:,j);
        % prP(i,j) is the prob changing from pi to pj, multiply with
        % corresponding (V(:,:,j)) 
        end
    DEV(:,:,i)=d*(sum(junk,3)); % so this is the discounted expected value
    RHS(:,:,i)=periodr(:,:,i)+repmat(DEV(:,:,i)',1,nA);
    v(:,:,i)=max(RHS(:,:,i));
    diff=max(abs(v(:,:,i)-V(:,:,i)));
        if diff<eps
            disp('converged');
            break
        else
            V(:,:,i)=v(:,:,i);
        end
    end
end
% now back up policy
for i=1:nP
    [~,m(1,:,i)]=max(RHS(:,:,i));
end
%%% Question 4
% 0.9 1 1.1 may not directly live on gridp, I need to find the closest one
% of gridp to 0.9 1 1.1 respectively
p_Q3=[0.9,1,1.1];
dist=[gridP;gridP;gridP]-p_Q3'*ones(1,nP);
for pi=1:3
    
    [~,indp_Q3(pi)]=min(abs(dist(pi,:)));
end
% so we are plotting for p=gridP(indp_Q3));
figure (1)
plot(gridA,V(1,:,indp_Q3(1)),gridA,V(1,:,indp_Q3(2)),gridA,V(1,:,indp_Q3(3)))
legend('p=0.9','p=1','p=1.1')
xlabel('current stock')
title('vfun for p=0.9,1,1.1');

% choose different stock: gridA(10),gridA(50),gridA(90)
for i=1:nP
    choice(i,:)=gridA(m(1,:,i));% recovering from m index to choice, value
    choiceind(i,:)=m(1,:,i);
end

figure (2)
subplot(2,1,1)
% here gridA(1) corresponds to A=0 and gridA(201) corresponds to A=100. be
% careful between index and actual amount
plot(gridP,repmat(gridA(1:10:201),nP,1)-choice(:,1:10:201));
xlabel('today price')
title('harvest amount as a func of curr p');
subplot(2,1,2)
plot(gridP,choice(:,1:10:201));
xlabel('today price')
title('optimal next stock as a func of curr p');

%%% Question 5
% this question basically asks us to simulate some data;
% we only have aggregate shock, so calculate p series for 100 times and see
% what the firm's choice is. 
% ========simulate markov process====================
T=20
rng(2)
epsi=rand(100,T);
N=length(prP);
% initial values of the indicator
z=zeros(100,T);
if N==21
    z(:,1)=11; %corresponds to p=1 at T=1, this index is different when N=5,so adjusted below
elseif N==5
    z(:,1)=3;
end
% calculate the accumulative prob
for r=1:N
Pcumu(r,:)=cumsum(prP(r,:));
end
% now the next state is j if the random draw is between pcumu(i,j) and
% pcumu(i,j+1)
for i=1:100
    for t=2:T
        if epsi(i,t)<Pcumu(z(t-1),1)
            z(i,t)=1;
        else
            k=find(Pcumu(z(t-1),:)<epsi(i,t));
            z(i,t)=max(k)+1;
        end
    end
end

% Hence the stock chosen will be
for h=1:100
        simu_A(h,1)=201; % gridA(201) correspond to stock=100
    for t=2:T
        simu_A(h,t)=choiceind(z(h,t),simu_A(h,t-1));
    end
end
expcA=mean(gridA(simu_A)); % this is the expected stock over time
for t=1:20
    rankA(:,t)=sort(gridA(simu_A(:,t)));
    % in order to pick the 90 firms in between, I take the 6th and the 95th
    % firm to construct the 90th interval
    top6(t)=rankA(6,t);
    top95(t)=rankA(95,t);
end

tt=1:1:T;
figure(3)
plot(tt,expcA,'r',tt,top6,'k:',tt,top95,'k:')
xlabel('time')
legend('Expected Stock','90% CI');

