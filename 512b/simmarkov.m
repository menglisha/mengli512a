function y=simmarkov(T,P)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mengli Sha Jan 2017
% This function simulates sequence of shocks which follows N state markov
% process after dropping the first 500 draws
% input:
%        T, number of periods 
%        P, transition matrix
% output: sequence of realized states (indicator)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% probability of changing from state i to j is P(i,j)
% draw shocks from uniform [0,1]; draw from the same seed
% rng(1);% this is to have same realizations for each beta
T=T+500;
epsi=rand(T,1);
N=length(P);
% initial values of the indicator
zz=zeros(T,1);
zz(1)=2;
% calculate the accumulative prob
for r=1:N
Pcumu(r,:)=cumsum(P(r,:));
end
% now the next state is j if the random draw is between pcumu(i,j) and
% pcumu(i,j+1)
for t=2:T
    if epsi(t)<Pcumu(zz(t-1),1)
        zz(t)=1;
    else
        k=find(Pcumu(zz(t-1),:)<epsi(t));
        zz(t)=max(k)+1;
    end
end
y=z(501:end);
end
    