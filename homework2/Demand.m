function F= Demand(x)
%first order conditon 1-pj(1-qj)=0 for all j
global vA vB vC
pA=x(1);
pB=x(2);
pC=x(3);
F(1) = exp(vA-pA)/(1+exp(vA-pA)+exp(vB-pB)+exp(vC-pC));
F(2) = exp(vB-pB)/(1+exp(vA-pA)+exp(vB-pB)+exp(vC-pC));
F(3) = exp(vC-pC)/(1+exp(vA-pA)+exp(vB-pB)+exp(vC-pC));
F=[F(1);F(2);F(3)];
end
