function F= DemandQ5(x)
%first order conditon 1-pj(1-qj)=0 for all j
global A B C
pA=x(1);
pB=x(2);
pC=x(3);
F(1) = exp(A-pA)/(1+exp(A-pA)+exp(B-pB)+exp(C-pC));
F(2) = exp(B-pB)/(1+exp(A-pA)+exp(B-pB)+exp(C-pC));
F(3) = exp(C-pC)/(1+exp(A-pA)+exp(B-pB)+exp(C-pC));
F=[F(1);F(2);F(3)];
end
