%%Econ 512a HW2
%%Mengli Sha
clear all

%%Q1 
%%vj=-1 for all j; in order to use v values in func, I use global
global vA vB vC 
vA=-1;
vB=-1;
vC=-1;
pA=1;
pB=1;
pC=1;
qA_Q1=(exp(vA-pA))/(1+exp(vA-pA)+exp(vB-pB)+exp(vC-pC))
qB_Q1=(exp(vB-pB))/(1+exp(vA-pA)+exp(vB-pB)+exp(vC-pC))
qC_Q1=(exp(vC-pC))/(1+exp(vA-pA)+exp(vB-pB)+exp(vC-pC))
% Comment from TA: when you want to break your matlab code into sections
% you put double percent sign, then space ant then name of section like I
% corrected in the next line. THen you can run those sections separately
%% Q2  
%%xa xb xc xd are the initial values, fval is the corresponding function
%%value (convergence criteria); the tol is set to be sqrt(eps)
tic
xa=[1;1;1]
[xstara,fvala,flaga,ita,fjacinva] = broyden(@myfunc,xa)
xb=[0;0;0]
[xstarb,fvalb,flagb,itb,fjacinvb] = broyden(@myfunc,xb)
xc=[0;1;2]
[xstarc,fvalc,flagc,itc,fjacinvc] = broyden(@myfunc,xc)

xd=[3;2;1]
[xstard,fvald,flagd,itd,fjacinvd] = broyden(@myfunc,xd)
toc
timeforbroyden=toc;

%% Q3
%%Define the functions
% Comment from TA: here in definition of function you use the same name as
% you use in main program and do not overwrite it with name that you
% provide from main program. That breaks Gauss-Seidel logic and your
% iterations lead to different solution. See answer key for clarification.
    FA =@(pA) (1-(exp(vA-pA))/(1+exp(vA-pA)+exp(vB-pB)+exp(vC-pC)))*pA-1;
    FB =@(pB) (1-(exp(vB-pB))/(1+exp(vA-pA)+exp(vB-pB)+exp(vC-pC)))*pB-1;
    FC =@(pC) (1-(exp(vC-pC))/(1+exp(vA-pA)+exp(vB-pB)+exp(vC-pC)))*pC-1;
%%For each initial guess, we need put two inital guess values, i put [0.9;0.9;0.9]
tic
%for initial guess(a)
xa=[1;1;1];
pA=[0.9;xa(1)];
pB=[0.9;xa(2)];
pC=[0.9;xa(3)];
for ite=1:1000
    %solving for each F(i)
    %use secant method for each loop
        for t=2:1000
            p=pA(t);
            if abs(FA(p))<sqrt(eps)
                disp('root found for F(A)')
                pAstar(ite)=pA(t)
                break 
            elseif abs(FA(p))>sqrt(eps)
                pA(t+1)=pA(t)-((pA(t)-pA(t-1))/(FA(pA(t))-FA(pA(t-1))))*FA(pA(t));
                if abs(pA(t+1)-pA(t))<sqrt(eps) %stop the loop if it does not seem to converge
                    break
                disp('the loop stuck')
                end
            end
        end
        for t=2:1000
            p=pB(t);
            pA=pAstar(ite); %updating 
            if abs(FB(p))<sqrt(eps)
                disp('root found for F(B)')
                pBstar(ite)=pB(t)
                 break 
            elseif abs(FB(p))>sqrt(eps)
                pB(t+1)=pB(t)-((pB(t)-pB(t-1))/(FB(pB(t))-FB(pB(t-1))))*FB(pB(t));
                if abs(pB(t+1)-pB(t))<sqrt(eps) %stop the loop if it does not seem to converge
                    break
                disp('the loop stuck')
                end
            end
        end
         for t=2:1000
            p=pC(t);
            pB=pBstar(ite); %updating
            pA=pAstar(ite); %updating 
            if abs(FC(p))<sqrt(eps)
                disp('root found for F(C)')
                pCstar(ite)=pC(t)
                break 
            elseif abs(FC(p))>sqrt(eps)
                pC(t+1)=pC(t)-((pC(t)-pC(t-1))/(FC(pC(t))-FC(pC(t-1))))*FC(pC(t));
                if abs(pC(t+1)-pC(t))<sqrt(eps)  %stop the loop if it does not seem to converge
                    break
                disp('the loop stuck')
                end
            end
        end
    if norm([FA(pAstar(ite));FB(pBstar(ite));FC(pCstar(ite))])<sqrt(eps)
        display('root found for the whole system')
        Roota=[pAstar(ite);pBstar(ite);pCstar(ite)]
        break
    elseif norm([FA(pAstar(ite));FB(pBstar(ite));FC(pCstar(ite))])>sqrt(eps)
            ite=ite+1;
            for itee=2:1000
                 %stop the loop if it does not seem to converge
                if norm([pAstar(itee)-pAstar(itee-1),pBstar(itee)-pBstar(itee-1),pCstar(itee)-pCstar(itee-1)])<sqrt(eps)
                    break
                    disp('system stuck')
                end
            end
    end
end
%for initial guess b
pA=[0.9;xb(1)];
pB=[0.9;xb(2)];
pC=[0.9;xb(3)];
for ite=1:1000
    %solving for each F(i)
    %use secant method for each loop
        for t=2:1000
            p=pA(t);
            if abs(FA(p))<sqrt(eps)
                disp('root found for F(A)')
                pAstar(ite)=pA(t)
                break 
            elseif abs(FA(p))>sqrt(eps)
                pA(t+1)=pA(t)-((pA(t)-pA(t-1))/(FA(pA(t))-FA(pA(t-1))))*FA(pA(t));
                if abs(pA(t+1)-pA(t))<sqrt(eps) %stop the loop if it does not seem to converge
                    break
                disp('the loop stuck')
                end
            end
        end
        for t=2:1000
            p=pB(t);
            pA=pAstar(ite); %updating 
            if abs(FB(p))<sqrt(eps)
                disp('root found for F(B)')
                pBstar(ite)=pB(t)
                 break 
            elseif abs(FB(p))>sqrt(eps)
                pB(t+1)=pB(t)-((pB(t)-pB(t-1))/(FB(pB(t))-FB(pB(t-1))))*FB(pB(t));
                if abs(pB(t+1)-pB(t))<sqrt(eps) %stop the loop if it does not seem to converge
                    break
                disp('the loop stuck')
                end
            end
        end
         for t=2:1000
            p=pC(t);
            pB=pBstar(ite); %updating
            pA=pAstar(ite); %updating 
            if abs(FC(p))<sqrt(eps)
                disp('root found for F(C)')
                pCstar(ite)=pC(t)
                break 
            elseif abs(FC(p))>sqrt(eps)
                pC(t+1)=pC(t)-((pC(t)-pC(t-1))/(FC(pC(t))-FC(pC(t-1))))*FC(pC(t));
                if abs(pC(t+1)-pC(t))<sqrt(eps)  %stop the loop if it does not seem to converge
                    break
                disp('the loop stuck')
                end
            end
        end
    if norm([FA(pAstar(ite));FB(pBstar(ite));FC(pCstar(ite))])<sqrt(eps)
        display('root found for the whole system')
        Rootb=[pAstar(ite);pBstar(ite);pCstar(ite)]
        break
    elseif norm([FA(pAstar(ite));FB(pBstar(ite));FC(pCstar(ite))])>sqrt(eps)
            ite=ite+1;
            for itee=2:1000
                 %stop the loop if it does not seem to converge
                if norm([pAstar(itee)-pAstar(itee-1),pBstar(itee)-pBstar(itee-1),pCstar(itee)-pCstar(itee-1)])<sqrt(eps)
                    break
                    disp('system stuck')
                end
            end
    end
end
%for initial guess c
pA=[0.9;xc(1)];
pB=[0.9;xc(2)];
pC=[0.9;xc(3)];
for ite=1:1000
    %solving for each F(i)
    %use secant method for each loop
        for t=2:1000
            p=pA(t);
            if abs(FA(p))<sqrt(eps)
                disp('root found for F(A)')
                pAstar(ite)=pA(t)
                break 
            elseif abs(FA(p))>sqrt(eps)
                pA(t+1)=pA(t)-((pA(t)-pA(t-1))/(FA(pA(t))-FA(pA(t-1))))*FA(pA(t));
                if abs(pA(t+1)-pA(t))<sqrt(eps) %stop the loop if it does not seem to converge
                    break
                disp('the loop stuck')
                end
            end
        end
        for t=2:1000
            p=pB(t);
            pA=pAstar(ite); %updating 
            if abs(FB(p))<sqrt(eps)
                disp('root found for F(B)')
                pBstar(ite)=pB(t)
                 break 
            elseif abs(FB(p))>sqrt(eps)
                pB(t+1)=pB(t)-((pB(t)-pB(t-1))/(FB(pB(t))-FB(pB(t-1))))*FB(pB(t));
                if abs(pB(t+1)-pB(t))<sqrt(eps) %stop the loop if it does not seem to converge
                    break
                disp('the loop stuck')
                end
            end
        end
         for t=2:1000
            p=pC(t);
            pB=pBstar(ite); %updating
            pA=pAstar(ite); %updating 
            if abs(FC(p))<sqrt(eps)
                disp('root found for F(C)')
                pCstar(ite)=pC(t)
                break 
            elseif abs(FC(p))>sqrt(eps)
                pC(t+1)=pC(t)-((pC(t)-pC(t-1))/(FC(pC(t))-FC(pC(t-1))))*FC(pC(t));
                if abs(pC(t+1)-pC(t))<sqrt(eps)  %stop the loop if it does not seem to converge
                    break
                disp('the loop stuck')
                end
            end
        end
    if norm([FA(pAstar(ite));FB(pBstar(ite));FC(pCstar(ite))])<sqrt(eps)
        display('root found for the whole system')
        Rootc=[pAstar(ite);pBstar(ite);pCstar(ite)]
        break
    elseif norm([FA(pAstar(ite));FB(pBstar(ite));FC(pCstar(ite))])>sqrt(eps)
            ite=ite+1;
            for itee=2:1000
                 %stop the loop if it does not seem to converge
                if norm([pAstar(itee)-pAstar(itee-1),pBstar(itee)-pBstar(itee-1),pCstar(itee)-pCstar(itee-1)])<sqrt(eps)
                    break
                    disp('system stuck')
                end
            end
    end
end
%tic
%%for initial guess (d)
pA=[0.9;xd(1)];
pB=[0.9;xd(2)];
pC=[0.9;xd(3)];
for ite=1:1000
    %solving for each F(i)
    %use secant method for each loop
        for t=2:1000
            p=pA(t);
            if abs(FA(p))<sqrt(eps)
                disp('root found for F(A)')
                pAstar(ite)=pA(t)
                break 
            elseif abs(FA(p))>sqrt(eps)
                pA(t+1)=pA(t)-((pA(t)-pA(t-1))/(FA(pA(t))-FA(pA(t-1))))*FA(pA(t));
                if abs(pA(t+1)-pA(t))<sqrt(eps) %stop the loop if it does not seem to converge
                    break
                disp('the loop stuck')
                end
            end
        end
        for t=2:1000
            p=pB(t);
            pA=pAstar(ite); %updating 
            if abs(FB(p))<sqrt(eps)
                disp('root found for F(B)')
                pBstar(ite)=pB(t)
                 break 
            elseif abs(FB(p))>sqrt(eps)
                pB(t+1)=pB(t)-((pB(t)-pB(t-1))/(FB(pB(t))-FB(pB(t-1))))*FB(pB(t));
                if abs(pB(t+1)-pB(t))<sqrt(eps) %stop the loop if it does not seem to converge
                    break
                disp('the loop stuck')
                end
            end
        end
         for t=2:1000
            p=pC(t);
            pB=pBstar(ite); %updating
            pA=pAstar(ite); %updating 
            if abs(FC(p))<sqrt(eps)
                disp('root found for F(C)')
                pCstar(ite)=pC(t)
                break 
            elseif abs(FC(p))>sqrt(eps)
                pC(t+1)=pC(t)-((pC(t)-pC(t-1))/(FC(pC(t))-FC(pC(t-1))))*FC(pC(t));
                if abs(pC(t+1)-pC(t))<sqrt(eps)  %stop the loop if it does not seem to converge
                    break
                disp('the loop stuck')
                end
            end
        end
    if norm([FA(pAstar(ite));FB(pBstar(ite));FC(pCstar(ite))])<sqrt(eps)
        display('root found for the whole system')
        Rootd=[pAstar(ite);pBstar(ite);pCstar(ite)]
        break
    elseif norm([FA(pAstar(ite));FB(pBstar(ite));FC(pCstar(ite))])>sqrt(eps)
            ite=ite+1;
            for itee=2:1000
                 %stop the loop if it does not seem to converge
                if norm([pAstar(itee)-pAstar(itee-1),pBstar(itee)-pBstar(itee-1),pCstar(itee)-pCstar(itee-1)])<sqrt(eps)
                    break
                    disp('system stuck')
                end
            end
    end
end
toc
timeforsidel=toc;
%it terms out for the same guess (d), sidel costs more seconds than
%broyden when they both found roots; but if I calculated the same for all
%the four guesses (abcd) then broyden takes more time than the sidel
%method. So i think on average broyden method would take more time.
%this comparison makes sense because both the tol are set to be sqrt(eps) and both are using the same intial
%guesses
%The reason is that Broyden's method calculates the jacobian matrix (in
%this case it is 3 by 3) which is costly in each iteration but for the
%sidel, it solves equations 1 by 1 and each time it only needs to calculate
%one derivatve: a scalar.

% Comment from TA: you should have been warned that solutions are different
% by 0.02, which is much more than tolerance
%% Q4
tic
% for intitial guess a
for t=1:1000
    xa4(:,1)=xa;%initial guess
    x=xa4(:,t);
    if norm(xa4(:,t)-(1-Demand(x)).^(-1))<sqrt(eps)
        disp('Solution found')
        paQ4=xa4(:,t)
        break
    elseif norm(xa4(:,t)-(1-Demand(x)).^(-1))>sqrt(eps) 
        xa4(:,t+1)=((1-Demand(x)).^(-1));
        if norm(xa4(:,t+1)-xa4(:,t))<sqrt(eps) %stop if it does not seem to converge
            disp('loop stuck');
        break
        end
    end
end

% for intitial guess b
for t=1:1000
    xb4(:,1)=xb;%initial guess
    x=xb4(:,t);
    if norm(xb4(:,t)-(1-Demand(x)).^(-1))<sqrt(eps)
        disp('Solution found')
        pbQ4=xb4(:,t)
        break
    elseif norm(xb4(:,t)-(1-Demand(x)).^(-1))>sqrt(eps) 
        xb4(:,t+1)=((1-Demand(x)).^(-1));
        if norm(xb4(:,t+1)-xb4(:,t))<sqrt(eps) %stop if it does not seem to converge
            disp('loop stuck');
        break
        end
    end
end

% for initial guess c
for t=1:1000
    xc4(:,1)=xc;%initial guess
    x=xc4(:,t);
    if norm(xc4(:,t)-(1-Demand(x)).^(-1))<sqrt(eps)
        disp('Solution found')
        pcQ4=xc4(:,t)
        break
    elseif norm(xc4(:,t)-(1-Demand(x)).^(-1))>sqrt(eps) 
        xc4(:,t+1)=((1-Demand(x)).^(-1));
        if norm(xc4(:,t+1)-xc4(:,t))<sqrt(eps) %stop if it does not seem to converge
            disp('loop stuck');
        break
        end
    end
end

% for initial guess d
%tic
for t=1:1000
    xd4(:,1)=xd;%initial guess
    x=xd4(:,t);
    if norm(xd4(:,t)-(1-Demand(x)).^(-1))<sqrt(eps)
        disp('Solution found')
        pdQ4=xd4(:,t)
        break
    elseif norm(xd4(:,t)-(1-Demand(x)).^(-1))>sqrt(eps) 
        xd4(:,t+1)=((1-Demand(x)).^(-1));
        if norm(xd4(:,t+1)-xd4(:,t))<sqrt(eps) %stop if it does not seem to converge
            disp('loop stuck');
        break
        end
    end
end
toc
timeforBR4=toc
%this converges and it turns out that this is faster than broyden and sidel
%because 


%% Q5
%I prefer the method in Q4 because it is reasonable we know that nash equilibrium 
%is a fixed point of best responses. we already know that a unique equilibrium exists here;
%so we can get to the point after iterations and it is faster than solving
%for roots of a system of nonlinear quations as I did in the previous 3
%questions.
vA5=-1;
vB5=-1;
vC5=[-4,-3,-2,-1,0,1];
for i=1:length(vC5);
    global A B C
    A=vA5;
    B=vB5;
    C=vC5(i);
    %I defined demandQ5 as a function, which uses parameter values in Q5
    for t=1:1000
    xa5(:,1)=xa;%initial guess
    x=xa5(:,t);
        if norm(xa5(:,t)-(1-DemandQ5(x)).^(-1))<sqrt(eps)
        disp('Solution found')
        pQ5(:,i)=xa5(:,t)
            break
        elseif norm(xa5(:,t)-(1-DemandQ5(x)).^(-1))>sqrt(eps) 
        xa5(:,t+1)=((1-DemandQ5(x)).^(-1));
            if norm(xa5(:,t+1)-xa5(:,t))<sqrt(eps) %stop if it does not seem to converge
            disp('loop stuck');
            break
            end
        end
    end
end
pAQ5=pQ5(1,:);
pCQ5=pQ5(3,:);
plot(vC5,pAQ5,vC5,pCQ5)
legend('pA','pC')